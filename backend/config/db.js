'use strict';
const Mongoose = require('mongoose');
const Config = require('./config');
Mongoose.connect('mongodb://'+Config.database.host+'/'+Config.database.database);
const db = Mongoose.connection;
db.on('error',console.error.bind(console,'connection error'));
db.once('open',()=>{
 console.log("connection with database successful");
});
 
exports.mongoose = Mongoose;
exports.db = db;