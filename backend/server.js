"use strict";

const Hapi = require('hapi');
const Good = require('good');
const Path = require('path');
const mysql = require('mysql');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('../package');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const Config = require('./config/config');
const Routes = require('./routes/routes');
const Db =  require('./config/db');
const Mongoose = require('mongoose');

const server  = new Hapi.Server();
server.connection(Config.server);


const options = {
    info: {
            'title': 'Twitter API',
            'version': Pack.version,
        }
    };


server.route(Routes.endpoints);

server.register([
    Inert,
    Vision,
    {
        'register': HapiSwagger,
        'options': options
    }], (err) => {
        server.start( (err) => {
           if (err) {
                console.log(err);
            } else {
                console.log('Server running at:', server.info.uri);
            }
        });
});