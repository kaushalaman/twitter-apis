'use strict';
const Db = require('../config/db');
//const Routes = require('../route/routes');
const Joi = require('joi');
const User = require('../models/schema');

//const Tweet = require('../models/schema');
//console.log(User.find({}));
console.log("controller");
exports.get = {
	tags:['api'],
    description:'Home Page',
    notes:'Home Page of website',    
    handler: function(request, reply){
    reply.file("../../client/src/index.html");
}
};
    
exports.index = {
	tags:['api'],
	description:'Get all users',
	notes:'Get all users',
	handler:function(request,reply){
		console.log("find need to start");
		User.find({},function(err,res){
			
			if(err)
				throw err;
			if(!res)
				reply("no result");
			return reply(res);
		});
	}
};

exports.login = {
	tags:['api'],
	description:'Login Check',
	notes:'Login Check',
	validate:{
		payload:{
			id:Joi.string().required(),
			password:Joi.string().required()
		}
	},
	handler:function(request,reply)
	{
		User.findOne({$or: [{email:request.payload.id},{username:request.payload.id}]},function(err,user){
			
			if(err)
				throw err;
			if(!user)
			{
				reply({
					statuscode:200,
					message:'User not found'
				});
			}
			else
			{
				user.comparePassword(request.payload.password,function(err,isMatch){
					
					if(err)
						throw err;
					else if(isMatch)
					{
						reply({
							statusCode:200,
							message:'Login Successful',
							res:isMatch,
							data:"Welcome "+request.payload.id
						})
					}
					else{
						reply({
							message:'Login Failed',
							res:isMatch,
							data:"Sorry "
						})
					}
				});
			}
		});
					
	}
};

exports.register = {
	tags:['api'],
	description:'Register check',
	notes:'Register Check',
	validate:{
		payload:{
			Name:Joi.string().required(),
			email:Joi.string().required(),
			username:Joi.string().required(),
			password:Joi.string().required(),
			contact:Joi.string().required(),
			age: Joi.number().required(),
			//gender: Joi.string().required()
		}
	},
	handler:function(request,reply)
	{
		let user = new User({
			names:request.payload.Name,
			email:request.payload.email,
			password:request.payload.password,
			username:request.payload.username,
			contact:request.payload.contact,
			age:request.payload.age

		});
	
		user.save(user,(err)=>{
			
			if(err)
			{
				reply({
					statusCode:503,
					message:'User not inserted',
					data:err
				});
			}
			else
			{
				reply({
					statusCode:201,
					message:'User is inserted'
				});
			}
		});
	}
};

exports.delete = {
	tags:['api'],
	description:'Delete all users',
	notes:'Delete all users',
	handler:function(request,reply)
	{
		User.remove({},(err)=>{
			
			if(err){
				reply({
					statuscode:503,
					message:'Problem in deleting users'
				});
			}
			else
			{
				reply({
					statusCode:204,
					message:'All users deleted'
				})
			}
		});
	}
};

exports.logout = {
	handler:function(request,reply)
	{
		reply("all clear");
	}

};
