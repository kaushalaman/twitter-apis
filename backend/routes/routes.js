'use strict';
const Controller = require('../controller/user.js');
exports.endpoints = [
	{method:'GET',path:'/twitter/api/home',config:Controller.get},
	{method:'GET', path:'/twitter/api/',config:Controller.index},
	{method:'POST', path:'/twitter/api/login',config:Controller.login},
	{method:'POST', path:'/twitter/api/register',config:Controller.register},
	{method:'GET',path:'/twitter/api/logout',config:Controller.logout},
	{method:'DELETE',path:'/twitter/api/delete',config:Controller.delete}
];